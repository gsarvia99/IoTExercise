#if __name__=="main":
name = "Jacomo"
age = 21
gg = 27
mm = 10
aaaa = 1999

    #First method
print("1)   I'm %s and I'm %d years old, I was born in %d/%d/%d" %(name, age, gg,mm,aaaa))

    #Second method
print(f"2)  I'm {name} and I'm {age } years old, I was born in {gg}/{mm}/{aaaa}")

print("Please help me knowing you:")
your_name = input("What's your name?")
your_age = input("What's your age?")

print(f"So you are {your_name} and you are {your_age}, nice to meet you!")

#pass